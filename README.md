# MONROE-LTE

This is a repository for the MONROE software extension known as MONROE-LTE (Affordable LTE Network Benchmarking Based on MONROE).

More specifically, this repository provides sample templates for experiments and the open source code for the MONROE-LTE dashboard.
