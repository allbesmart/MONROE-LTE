This script allows the user to capture the main video KPIs (time-to-start, avg. bitrate, number of stalling events, total stalling time, video frames lost, audio buffers lost)). By default, a short 4K video is selected (Big Buck Bunny). It is possible to change the video by using the -u flag as:
e.g., python video-kpis.py -u <YoutubeUrl>

In order to successfully run this application:
    1. Make sure python and python-pip are installed, if not run first:
sudo apt-get install -y python python-pip

    2. Install video-kpis requirements as follows:
sudo pip install -r requirements.txt

    3. Run video-kpis:
e.g., python video-kpis.py

    4. To see help/menu:
e.g., python video-kpis.py -h
