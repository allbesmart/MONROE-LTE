#!/usr/bin/python
import re
import sys
import json
import time
import curses
import argparse
import subprocess

#**********************************************************************************************
#Developped and tested under Ubuntu 17.10 and VLC 2.2.6 Umbrella (revision 2.2.6-0-g1aae78981c)
#Requirements in adjacent file requirements-yt-wrapper.txt
#Install requirements with pip install requirements-yt-wrapper.txt
#Run python yt-wrapper.py -h to see the help menu
#**********************************************************************************************

def main(  ):
    screen = curses.initscr()
    curses.curs_set(0)
    startTime = referenceTime = time.time()

    # Starts a vlc instance and enables the remote control for automation
    p = subprocess.Popen([
        '/usr/bin/vlc', arguments.url,
        '--verbose', '2',
        '--intf', 'rc',
        '--stats', '--fullscreen'
    ],
     stdin=subprocess.PIPE,
     stdout=subprocess.PIPE,
     stderr=subprocess.STDOUT,
     bufsize=1
    )


    timeToStart = 0

    stalled = False
    stallingEvents = -1
    totalStallingTime = 0

    averageBitrate = [0, 0]

    audioBuffersLost = 0
    audioBuffersDecoded = 0

    videoFramesLost = 0
    videoFramesDecoded = 0

    outLine = p.stdout.readline()

    lostFramesString = "Lost Frames: "
    lostBuffersString = "Lost Buffers: "
    timeToStartString = "Time To Start: "
    stallingEventsString = "Stalling Events: "
    averageBitrateString = "Average Bitrate: "
    instantBitrateString = "Instant Bitrate: "

    screen.addstr(0,0, lostFramesString)
    screen.addstr(1,0, lostBuffersString)
    screen.addstr(2,0, timeToStartString)
    screen.addstr(3,0, stallingEventsString)
    screen.addstr(4,0, averageBitrateString)
    screen.addstr(5,0, instantBitrateString)
    
    #When video ends, the decoder is killed and the output states 'killing decoder' wich means the job is done and vlc should be killed
    while not 'killing decoder' in outLine:
        outLine = p.stdout.readline()

        currentTime = int(time.time())
        if currentTime >= int(referenceTime) + 1:
            print >>p.stdin, 'stats'
            p.stdin.flush()
            referenceTime = int(time.time())

        if outLine:
            #Time to start calculation
            if 'core decoder debug: Received first picture' in outLine and timeToStart == 0:
                timeToStart = int( (time.time() - startTime) * 1000 )
                screen.addstr( 2, len(timeToStartString), str( "{} ms".format( int( (time.time() - startTime) * 1000 ) ) ) )
                screen.clrtoeol()
                screen.refresh()

            #Each time vlc freezes, a buffering is emitted which produces the following output
            if 'core input debug: Buffering 0%' in outLine and not stalled:
                stalled = True
                stallingEvents = stallingEvents + 1
                screen.addstr( 3, len(stallingEventsString), str( "{}".format( stallingEvents ) ) )
                screen.clrtoeol()
                screen.refresh()

            #When buffering is done following ouput is emitted
            if 'core input debug: Stream buffering done' in outLine and stalled:
                stalled = False
                totalStallingTime = totalStallingTime + int( outLine.split('(')[1].split(')')[0].split()[3] )

            try:
                parsedLine = ' '.join(re.split(' |\r\n',outLine.split(':')[1])).split()
                #Average bitrate calculation
                if 'input bitrate' in outLine:
                    if parsedLine[1] == 'kb/s' and int(parsedLine[0]) > 0:
                        averageBitrate[1] = averageBitrate[1] + 1
                        averageBitrate[0] = averageBitrate[0] + int(parsedLine[0])
                        screen.addstr( 4, len(averageBitrateString), str( "{} kbps".format( float(averageBitrate[0]/averageBitrate[1]) ) ) )
                        screen.clrtoeol()
                        screen.refresh()
                        screen.addstr( 5, len(instantBitrateString), str( "{} kbps".format( int(parsedLine[0]) ) ) )
                        screen.clrtoeol()
                        screen.refresh()

                if 'audio decoded' in outLine:
                    audioBuffersDecoded = int(parsedLine[0])

                if 'buffers played' in outLine:
                    pass

                #lost buffers
                if 'buffers lost' in outLine:
                    audioBuffersLost = int(parsedLine[0])
                    screen.addstr( 1, len( lostBuffersString ), str( "{}".format( audioBuffersLost ) ) )
                    screen.clrtoeol()
                    screen.refresh()

                if 'video decoded' in outLine:
                    videoFramesDecoded = int(parsedLine[0])

                if 'frames displayed' in outLine:
                    pass

                #lost frames
                if 'frames lost' in outLine:
                    videoFramesLost = int(parsedLine[0])
                    screen.addstr( 0, len( lostFramesString ), str( "{}".format( videoFramesLost ) ) )
                    screen.clrtoeol()
                    screen.refresh()

            except Exception:
                pass
                
        curses.curs_set(0)

    print >>p.stdin, 'quit'
    p.stdin.flush()

    output = {}
    output['timeToStart'] = timeToStart
    output['stallingEvents'] = stallingEvents
    output['videoFramesLost'] = videoFramesLost
    output['audioBuffersLost'] = audioBuffersLost
    output['totalStallingTime'] = totalStallingTime
    output['videoFramesDecoded'] = videoFramesDecoded
    output['audioBuffersDecoded'] = audioBuffersDecoded
    output['averageBitrate'] = float(averageBitrate[0]/averageBitrate[1])
    output['videoFramesLostPercentage'] = float( float(videoFramesLost)/float(videoFramesDecoded) )*100
    output['audioBuffersLostPercentage'] = float( float(audioBuffersLost)/float(audioBuffersDecoded) )*100

    output['requestedUrl'] = arguments.url

    curses.endwin()

    #Outputs gathered results as json
    print(json.dumps(output))

    sys.exit(1)

if __name__ == '__main__':

    parser = argparse.ArgumentParser(description='Youtube Video QOS something something')
    parser.add_argument("-u", "--url", type=str, required=False, default="https://youtu.be/OfIQW6s1-ew")

    arguments = parser.parse_args()

    main()
