This bundle of files allows the user to capture the main web KPIs, when accessing a specific webpage, that are relevant to compute the web user experience (webUX). 

In order to successfully run this application:
    1. Make sure python and python-pip are installed, if not run first:
sudo apt-get install -y python python-pip

    2. Install google-chrome-stable
        2.1. ADD the key:
wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | sudo apt-key add –

        2.2. Set the repository:
echo 'deb [arch=amd64] http://dl.google.com/linux/chrome/deb/ stable main' | sudo tee /etc/apt/sources.list.d/google-chrome.list

        2.3. Update repositories and Install the package:
sudo apt-get update 
sudo apt-get install -y google-chrome-stable

    3. Install web-kpis.py requirements as follows:
sudo pip install -r requirements.txt

    4. Run web-kpis:
e.g.,: python web-kpis.py -u http://www.youtube.com
