#!/usr/bin/python
import os
import sys
import signal
import thread
import argparse
import requests
import websocket
import subprocess
import simplejson as json

from har import HAR
from dom import DOM
from page import Page
from time import time
from time import sleep
from runtime import Runtime
from datetime import datetime

class ChromeRDPWebsocket(object):
    command_id = 0

    def __init__(self, wsurl, target_url):
        self.debugging_url = wsurl
        self.page = None
        self.target_url = target_url

        self.ws = websocket.WebSocketApp(self.debugging_url,\
                                        on_message = self.on_message,\
                                        on_error = self.on_error,\
                                        on_close = self.on_close)
        self.ws.on_open = self.on_open
        self.ws.run_forever() # start running this socket.

    def load_url(self, url):
        index = 0
        self.page = Page(index, url, self.ws, fetch_content=True)
        self.navigate_to_page(url)

    def on_message(self, ws, message):

        '''
        Handle each message.
        '''
        try:
            message_obj = json.loads(message)

            if self.page:
                self.page.process_message(message_obj)
                if self.page.finished:
                    self.close_connection()
        except Exception as e:
            pass

    def on_error(self, ws, error):
        pass

    def on_close(self, ws):
        pass

    def on_open(self, ws):
        self.navigate_to_page('about:blank')
        self.enable_network_tracking()
        self.enable_page_tracking()
        self.load_url(self.target_url)

    def close_connection(self):
        self.ws.close()
    def clear_cache(self):
        self.enqueue_command(method='Network.clearBrowserCache')

    def can_clear_cache(self):
        self.enqueue_command(method='Network.canClearBrowserCache')

    def disable_network_tracking(self):
        self.enqueue_command(method='Network.disable')

    def disable_page_tracking(self):
        self.enqueue_command(method='Page.disable')

    def enable_network_tracking(self):
        self.enqueue_command(method='Network.enable')
        self.enqueue_command(method='Network.setCacheDisabled', params={"cacheDisabled": True})

    def enable_page_tracking(self):
        self.enqueue_command(method='Page.enable')

    def enable_trace_collection(self):
        self.enqueue_command(method='Tracing.start')

    def stop_trace_collection(self):
        self.enqueue_command(method='Tracing.end')

    def get_debugging_url(self):
        return self.debugging_url

    def navigate_to_page(self, url):
        self.enqueue_command(method='Page.navigate', params={"url": url})

    @property
    def next_command_id(self):
        self.command_id += 1
        return self.command_id

    # Async
    def enqueue_command(self, method, params={}, callback=None):
        msg = {'id': self.next_command_id, 'method': method, 'params': params}
        self.ws.send(json.dumps(msg))
        sleep(0.3)

if __name__ == '__main__':

    parser = argparse.ArgumentParser(description='Fetch har content with chrome automation')
    parser.add_argument("-u", "--url", type=str, required=True)
    parser.add_argument("-host", "--host", type=str, required=False, default="localhost")
    parser.add_argument("-p", "--port", type=str, required=False, default="9222")
    parser.add_argument("-t", "--timeout", type=int, required=False, default="30")

    arguments = parser.parse_args()
    headlessChromeProcess = subprocess.Popen(['/usr/bin/chromium-browser --remote-debugging-port={} --headless'.format(arguments.port)], stdout=subprocess.PIPE, shell=True, preexec_fn=os.setsid)

    startTime = int(time())
    while True:
        try:
            if int(time()) == startTime+arguments.timeout:
                output = {}
                output['output'] = {}
                output['output']['url'] = arguments.host
                output['output']['status'] = 408
                print(json.dumps(output))
                os.killpg(os.getpgid(headlessChromeProcess.pid), signal.SIGTERM)
                sys.exit(0)

            # find websocket endpoint
            response = requests.get("http://%s:%s/json" % (arguments.host, arguments.port))
            tablist = json.loads(response.text)

            wsdurl = tablist[0]['webSocketDebuggerUrl']

            # Fetch and render page
            client = ChromeRDPWebsocket(wsdurl, arguments.url)

            # Save as HAR file
            har = HAR()
            har.from_page(client.page)

            output = {}
            output['output'] = {}
            har.har['log']['pages'][0]['pageTimings']['url'] = arguments.url
            output['output']['totalDnsResolutionTime'] = 0
            output['output']['totalSslSessionEstablishmentTime'] = 0
            for entry in har.har['log']['entries']:
                output['output']['totalDnsResolutionTime'] = output['output']['totalDnsResolutionTime'] + entry['timings']['dns']
                output['output']['totalSslSessionEstablishmentTime'] = output['output']['totalSslSessionEstablishmentTime'] + entry['timings']['ssl']
                entry['startedDateTime'] = (datetime.strptime(str(entry['startedDateTime']),"%Y-%m-%dT%H:%M:%S.%fZ")- datetime(1970, 1, 1)).total_seconds()

            try:
                entriesList = sorted(har.har['log']['entries'], key=lambda k: k['startedDateTime'])

                output['output']['url'] = entriesList[0]['request']['url']
                output['output']['status'] = entriesList[0]['response']['status']
                output['output']['totalLoadTime'] = har.har['log']['pages'][0]['pageTimings']['onLoad']
                output['output']['domLoadTime'] = har.har['log']['pages'][0]['pageTimings']['onContentLoad']

                print(json.dumps(output))
            except Exception as e:
                print('some error stuff, run')

            os.killpg(os.getpgid(headlessChromeProcess.pid), signal.SIGTERM)

            sys.exit(0)

        except Exception as e:
            pass
