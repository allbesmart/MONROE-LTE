This script allows the user to capture the main network KPIs (download and upload throughput and round-trip-time delay). The best server (amongst the Ookla network) is chosen based on distance and latency before the download and upload tests commit.

In order to successfully run this application:
    1. Make sure python and python-pip are installed, if not run first:
sudo apt-get install -y python python-pip

    2. Install network-kpis requirements as follows:
sudo pip install -r requirements.txt

    3. Run network-kpis:
e.g., python network-kpis.py