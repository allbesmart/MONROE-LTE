#!/usr/bin/python

import json
import time
import speedtest

def main():
    speedtester = speedtest.Speedtest()

    #Chooses best server based on distance and best value for latency
    speedtester.get_best_server()
    #Performs download test against chosen server
    speedtester.download()  
    #Performs upload test against chosen server
    speedtester.upload()

    results = {}
    #epoch milisecond
    results['timestamp'] = int(time.time()*1000)
    #server id for possible troubleshooting
    results['host'] = speedtester.results.server['id']
    #round trip time value comes in ms
    results['roundTripTime'] = speedtester.results.ping
    #upload bitrate comes in bits/second
    results['uploadThorughput'] = speedtester.results.upload
    #download bitrate comes in bits/second
    results['downloadThroughput'] = speedtester.results.download

    print( json.dumps( results ) )

if __name__ == '__main__':
    main()