# MONROE-LTE Dashboard

This bundle of files allows the user to run the MONROE-LTE dashboard with functionalities such as heat map visualization and mobile network benchmarking.


## Prerequisites

The dashboard was developed on top of a web2py webserver, therefore a prerequisite is to install the web2py framework and postgresql database system.
* [web2py](http://www.web2py.com/init/default/documentation) - web framework
* [Postgresql](https://www.postgresql.org/docs/) - database system
* In models/db.py change the db string to match your setup: "db = DAL('postgres://username:password@localhost/dashboardDb')"


## Deployment

To run the server
```
  python web2py.py -i <ip> -p <port>
```