/**
 * Created by tiago on 20-Sep-17.
 */

function colorScaleDownload(){
   var colorScaleDW = $(
   '<table class="table table-condensed" style="width: 150px">'+
            '<thead>'+
               '<tr>'+
                   '<th style="font-weight: 500; text-align: center; font-size: 20px; ; text-shadow: 0px 0px 4px #000000; color: white;">'+
                       'Throughput [Mbit/s]'+
                   '</th>'+
               '</tr>'+
            '</thead>'+
            '<tbody>'+
               '<tr style="background-color: #FF0000;font-weight: 500; text-align: center; text-shadow: 0px 0px 4px #FFFFFF; color: black;">'+
                   '<td>'+
                       '[> 70]'+
                   '</td>'+
               '</tr>'+
               '<tr style="background-color: #FD3C04;font-weight: 500; text-align: center; text-shadow: 0px 0px 4px #FFFFFF; color: black;">'+
                   '<td>'+
                       '[60, 70]'+
                   '</td>'+
               '</tr>'+
               '<tr style="background-color: #FB7508;font-weight: 500; text-align: center; text-shadow: 0px 0px 4px #FFFFFF; color: black;">'+
                   '<td>'+
                       '[50, 60]'+
                   '</td>'+
               '</tr>'+
               '<tr style="background-color: #F9AC0C;font-weight: 500; text-align: center; text-shadow: 0px 0px 4px #FFFFFF; color: black;">'+
                   '<td>'+
                       '[46, 50]'+
                   '</td>'+
               '</tr>'+
               '<tr style="background-color: #F8E010;font-weight: 500; text-align: center; text-shadow: 0px 0px 4px #FFFFFF; color: black;">'+
                   '<td>'+
                       '[43, 46]'+
                   '</td>'+
               '</tr>'+
               '<tr style="background-color: #DBF615;font-weight: 500; text-align: center; text-shadow: 0px 0px 4px #FFFFFF; color: black;">'+
                   '<td>'+
                       '[40, 43]'+
                   '</td>'+
               '</tr>'+
               '<tr style="background-color: #A9F419;font-weight: 500; text-align: center; text-shadow: 0px 0px 4px #FFFFFF; color: black;">'+
                   '<td>'+
                       '[37, 40]'+
                   '</td>'+
               '</tr>'+
               '<tr style="background-color: #79F21D;font-weight: 500; text-align: center; text-shadow: 0px 0px 4px #FFFFFF; color: black;">'+
                   '<td>'+
                       '[34, 37]'+
                   '</td>'+
               '</tr>'+
               '<tr style="background-color: #4CF121;font-weight: 500; text-align: center; text-shadow: 0px 0px 4px #FFFFFF; color: black;">'+
                   '<td>'+
                       '[31, 34]'+
                   '</td>'+
               '</tr>'+
               '<tr style="background-color: #24EF28;font-weight: 500; text-align: center; text-shadow: 0px 0px 4px #FFFFFF; color: black;">'+
                   '<td>'+
                       '[28, 31]'+
                   '</td>'+
               '</tr>'+
               '<tr style="background-color: #28ED58;font-weight: 500; text-align: center; text-shadow: 0px 0px 4px #FFFFFF; color: black;">'+
                   '<td>'+
                       '[25, 28]'+
                   '</td>'+
               '</tr>'+
               '<tr style="background-color: #2CEC85;font-weight: 500; text-align: center; text-shadow: 0px 0px 4px #FFFFFF; color: black;">'+
                   '<td>'+
                       '[22, 25]'+
                   '</td>'+
               '</tr>'+
               '<tr style="background-color: #30EAB0;font-weight: 500; text-align: center; text-shadow: 0px 0px 4px #FFFFFF; color: black;">'+
                   '<td>'+
                       '[19, 22]'+
                   '</td>'+
               '</tr>'+
               '<tr style="background-color: #33E8D8;font-weight: 500; text-align: center; text-shadow: 0px 0px 4px #FFFFFF; color: black;">'+
                   '<td>'+
                       '[15, 19]'+
                   '</td>'+
               '</tr>'+
               '<tr style="background-color: #37CEE6;font-weight: 500; text-align: center; text-shadow: 0px 0px 4px #FFFFFF; color: black;">'+
                   '<td>'+
                       '[10, 15]'+
                   '</td>'+
               '</tr>'+
               '<tr style="background-color: #3AA7E5;font-weight: 500; color: black; text-align: center; text-shadow: 0px 0px 4px #FFFFFF;">'+
                   '<td>'+
                       '[7, 10]'+
                   '</td>'+
               '</tr>'+
               '<tr style="background-color: #3E82E3;font-weight: 500; color: black; text-align: center; text-shadow: 0px 0px 4px #FFFFFF;">'+
                   '<td>'+
                       '[4, 7]'+
                   '</td>'+
               '</tr>'+
               '<tr style="background-color: #4160E1;font-weight: 500; color: black; text-align: center; text-shadow: 0px 0px 4px #FFFFFF;">'+
                   '<td>'+
                       '[1, 4]'+
                   '</td>'+
               '</tr>'+
               '<tr style="background-color: #4A45E0; color: black; font-weight: 500; text-align: center; text-shadow: 0px 0px 4px #FFFFFF;">'+
                   '<td>'+
                       '[< 1]'+
                   '</td>'+
               '</tr>'+

            '</tbody>'+
   '</table>');
   return colorScaleDW[0];
}

function colorScaleRssi(){
 var colorScaleRssi = $(
   '<table class="table table-condensed" style="width: 150px">'+
            '<thead>'+
               '<tr>'+
                   '<th style="font-weight: 500; text-align: center; font-size: 20px; ; text-shadow: 0px 0px 4px #000000; color: white;">'+
                       '[dBm]'+
                   '</th>'+
               '</tr>'+
            '</thead>'+
            '<tbody>'+
               '<tr style="background-color: #FF0000;font-weight: 500; text-align: center; text-shadow: 0px 0px 4px #FFFFFF; color: black;">'+
                   '<td>'+
                       '[> -51]'+
                   '</td>'+
               '</tr>'+
               '<tr style="background-color: #FD3C04;font-weight: 500; text-align: center; text-shadow: 0px 0px 4px #FFFFFF; color: black;">'+
                   '<td>'+
                       '[-51, -54]'+
                   '</td>'+
               '</tr>'+
               '<tr style="background-color: #FB7508;font-weight: 500; text-align: center; text-shadow: 0px 0px 4px #FFFFFF; color: black;">'+
                   '<td>'+
                       '[-54, -58]'+
                   '</td>'+
               '</tr>'+
               '<tr style="background-color: #F9AC0C;font-weight: 500; text-align: center; text-shadow: 0px 0px 4px #FFFFFF; color: black;">'+
                   '<td>'+
                       '[-58, -62]'+
                   '</td>'+
               '</tr>'+
               '<tr style="background-color: #F8E010;font-weight: 500; text-align: center; text-shadow: 0px 0px 4px #FFFFFF; color: black;">'+
                   '<td>'+
                       '[-62, -66]'+
                   '</td>'+
               '</tr>'+
               '<tr style="background-color: #DBF615;font-weight: 500; text-align: center; text-shadow: 0px 0px 4px #FFFFFF; color: black;">'+
                   '<td>'+
                       '[-66, -70]'+
                   '</td>'+
               '</tr>'+
               '<tr style="background-color: #A9F419;font-weight: 500; text-align: center; text-shadow: 0px 0px 4px #FFFFFF; color: black;">'+
                   '<td>'+
                       '[-70, -74]'+
                   '</td>'+
               '</tr>'+
               '<tr style="background-color: #79F21D;font-weight: 500; text-align: center; text-shadow: 0px 0px 4px #FFFFFF; color: black;">'+
                   '<td>'+
                       '[-74, -78]'+
                   '</td>'+
               '</tr>'+
               '<tr style="background-color: #4CF121;font-weight: 500; text-align: center; text-shadow: 0px 0px 4px #FFFFFF; color: black;">'+
                   '<td>'+
                       '[-78, -82]'+
                   '</td>'+
               '</tr>'+
               '<tr style="background-color: #24EF28;font-weight: 500; text-align: center; text-shadow: 0px 0px 4px #FFFFFF; color: black;">'+
                   '<td>'+
                       '[-82, -86]'+
                   '</td>'+
               '</tr>'+
               '<tr style="background-color: #28ED58;font-weight: 500; text-align: center; text-shadow: 0px 0px 4px #FFFFFF; color: black;">'+
                   '<td>'+
                       '[-86, -90]'+
                   '</td>'+
               '</tr>'+
               '<tr style="background-color: #2CEC85;font-weight: 500; text-align: center; text-shadow: 0px 0px 4px #FFFFFF; color: black;">'+
                   '<td>'+
                       '[-90, -94]'+
                   '</td>'+
               '</tr>'+
               '<tr style="background-color: #30EAB0;font-weight: 500; text-align: center; text-shadow: 0px 0px 4px #FFFFFF; color: black;">'+
                   '<td>'+
                       '[-94, -98]'+
                   '</td>'+
               '</tr>'+
               '<tr style="background-color: #33E8D8;font-weight: 500; text-align: center; text-shadow: 0px 0px 4px #FFFFFF; color: black;">'+
                   '<td>'+
                       '[-98, -102]'+
                   '</td>'+
               '</tr>'+
               '<tr style="background-color: #37CEE6;font-weight: 500; text-align: center; text-shadow: 0px 0px 4px #FFFFFF; color: black;">'+
                   '<td>'+
                       '[-102, -104]'+
                   '</td>'+
               '</tr>'+
               '<tr style="background-color: #3AA7E5;font-weight: 500; color: black; text-align: center; text-shadow: 0px 0px 4px #FFFFFF;">'+
                   '<td>'+
                       '[-104, -108]'+
                   '</td>'+
               '</tr>'+
               '<tr style="background-color: #3E82E3;font-weight: 500; color: black; text-align: center; text-shadow: 0px 0px 4px #FFFFFF;">'+
                   '<td>'+
                       '[-108, -112]'+
                   '</td>'+
               '</tr>'+
               '<tr style="background-color: #4160E1;font-weight: 500; color: black; text-align: center; text-shadow: 0px 0px 4px #FFFFFF;">'+
                   '<td>'+
                       '[-112, -116]'+
                   '</td>'+
               '</tr>'+
               '<tr style="background-color: #4A45E0; color: black; font-weight: 500; text-align: center; text-shadow: 0px 0px 4px #FFFFFF;">'+
                   '<td>'+
                       '[< -116]'+
                   '</td>'+
               '</tr>'+

            '</tbody>'+
   '</table>');
 return colorScaleRssi[0];
}

function colorScaleRTT(){
   var colorScaleRTT = $(
     '<table class="table table-condensed" style="width: 150px">'+
              '<thead>'+
                 '<tr>'+
                     '<th style="font-weight: 500; text-align: center; font-size: 20px; ; text-shadow: 0px 0px 4px #000000; color: white;">'+
                         'Round Trip Time [ms]'+
                     '</th>'+
                 '</tr>'+
              '</thead>'+
              '<tbody>'+
                 '<tr style="background-color: #FF0000;font-weight: 500; text-align: center; text-shadow: 0px 0px 4px #FFFFFF; color: black;">'+
                     '<td>'+
                         '[ < 10 ]'+
                     '</td>'+
                 '</tr>'+
                 '<tr style="background-color: #FD3C04;font-weight: 500; text-align: center; text-shadow: 0px 0px 4px #FFFFFF; color: black;">'+
                     '<td>'+
                         '[10, 20]'+
                     '</td>'+
                 '</tr>'+
                 '<tr style="background-color: #FB7508;font-weight: 500; text-align: center; text-shadow: 0px 0px 4px #FFFFFF; color: black;">'+
                     '<td>'+
                         '[20, 30]'+
                     '</td>'+
                 '</tr>'+
                 '<tr style="background-color: #F9AC0C;font-weight: 500; text-align: center; text-shadow: 0px 0px 4px #FFFFFF; color: black;">'+
                     '<td>'+
                         '[30, 40]'+
                     '</td>'+
                 '</tr>'+
                 '<tr style="background-color: #F8E010;font-weight: 500; text-align: center; text-shadow: 0px 0px 4px #FFFFFF; color: black;">'+
                     '<td>'+
                         '[40, 50]'+
                     '</td>'+
                 '</tr>'+
                 '<tr style="background-color: #DBF615;font-weight: 500; text-align: center; text-shadow: 0px 0px 4px #FFFFFF; color: black;">'+
                     '<td>'+
                         '[50, 60]'+
                     '</td>'+
                 '</tr>'+
                 '<tr style="background-color: #A9F419;font-weight: 500; text-align: center; text-shadow: 0px 0px 4px #FFFFFF; color: black;">'+
                     '<td>'+
                         '[60, 70]'+
                     '</td>'+
                 '</tr>'+
                 '<tr style="background-color: #79F21D;font-weight: 500; text-align: center; text-shadow: 0px 0px 4px #FFFFFF; color: black;">'+
                     '<td>'+
                         '[70, 80]'+
                     '</td>'+
                 '</tr>'+
                 '<tr style="background-color: #4CF121;font-weight: 500; text-align: center; text-shadow: 0px 0px 4px #FFFFFF; color: black;">'+
                     '<td>'+
                         '[80, 90]'+
                     '</td>'+
                 '</tr>'+
                 '<tr style="background-color: #24EF28;font-weight: 500; text-align: center; text-shadow: 0px 0px 4px #FFFFFF; color: black;">'+
                     '<td>'+
                         '[90, 100]'+
                     '</td>'+
                 '</tr>'+
                 '<tr style="background-color: #28ED58;font-weight: 500; text-align: center; text-shadow: 0px 0px 4px #FFFFFF; color: black;">'+
                     '<td>'+
                         '[100, 150]'+
                     '</td>'+
                 '</tr>'+
                 '<tr style="background-color: #2CEC85;font-weight: 500; text-align: center; text-shadow: 0px 0px 4px #FFFFFF; color: black;">'+
                     '<td>'+
                         '[150, 200]'+
                     '</td>'+
                 '</tr>'+
                 '<tr style="background-color: #30EAB0;font-weight: 500; text-align: center; text-shadow: 0px 0px 4px #FFFFFF; color: black;">'+
                     '<td>'+
                         '[200, 250]'+
                     '</td>'+
                 '</tr>'+
                 '<tr style="background-color: #33E8D8;font-weight: 500; text-align: center; text-shadow: 0px 0px 4px #FFFFFF; color: black;">'+
                     '<td>'+
                         '[250, 300]'+
                     '</td>'+
                 '</tr>'+
                 '<tr style="background-color: #37CEE6;font-weight: 500; text-align: center; text-shadow: 0px 0px 4px #FFFFFF; color: black;">'+
                     '<td>'+
                         '[300, 350]'+
                     '</td>'+
                 '</tr>'+
                 '<tr style="background-color: #3AA7E5;font-weight: 500; color: black; text-align: center; text-shadow: 0px 0px 4px #FFFFFF;">'+
                     '<td>'+
                         '[350, 400]'+
                     '</td>'+
                 '</tr>'+
                 '<tr style="background-color: #3E82E3;font-weight: 500; color: black; text-align: center; text-shadow: 0px 0px 4px #FFFFFF;">'+
                     '<td>'+
                         '[400, 450]'+
                     '</td>'+
                 '</tr>'+
                 '<tr style="background-color: #4160E1;font-weight: 500; color: black; text-align: center; text-shadow: 0px 0px 4px #FFFFFF;">'+
                     '<td>'+
                         '[450, 500]'+
                     '</td>'+
                 '</tr>'+
                 '<tr style="background-color: #4A45E0; color: black; font-weight: 500; text-align: center; text-shadow: 0px 0px 4px #FFFFFF;">'+
                     '<td>'+
                         '[ > 500 ]'+
                     '</td>'+
                 '</tr>'+

              '</tbody>'+
     '</table>');
   return colorScaleRTT[0];
}

function downloadBitrateColor(downloadBitrate){
   var returnVar;
   if (downloadBitrate >= 70) {
       returnVar = '#FF0000'; //255,0,0
   } else if (60<=downloadBitrate && downloadBitrate<70) {
       returnVar = '#FD3C04'; //252,71,5
   } else if (50<=downloadBitrate && downloadBitrate<60) {
       returnVar = '#FB7508'; //250,139,10
   } else if (46<=downloadBitrate && downloadBitrate<50) {
       returnVar = '#F9AC0C'; //248,203,15
   } else if (43<=downloadBitrate && downloadBitrate<46) {
       returnVar = '#F8E010'; //229,246,20
   } else if (40<=downloadBitrate && downloadBitrate<43) {
       returnVar = '#DBF615'; //168,244,25
   } else if (37<=downloadBitrate && downloadBitrate<40) {
       returnVar = '#A9F419'; //111,242,29
   } else if (34<=downloadBitrate && downloadBitrate<37) {
       returnVar = '#79F21D'; //58,240,34
   } else if (31<=downloadBitrate && downloadBitrate<34) {
       returnVar = '#4CF121'; //39,238,69
   } else if (28<=downloadBitrate && downloadBitrate<31) {
       returnVar = '#24EF28'; //43,236,125
   } else if (25<=downloadBitrate && downloadBitrate<28) {
       returnVar = '#28ED58'; //48,234,177
   } else if (22<=downloadBitrate && downloadBitrate<25) {
       returnVar = '#2CEC85'; //52,232,225
   } else if (19<=downloadBitrate && downloadBitrate<22) {
       returnVar = '#30EAB0'; //56,190,230
   } else if (15<=downloadBitrate && downloadBitrate<19) {
       returnVar = '#33E8D8'; //60,144,228
   } else if (10<=downloadBitrate && downloadBitrate<15) {
       returnVar = '#37CEE6'; //64,102,226
   } else if (7<=downloadBitrate && downloadBitrate<10) {
       returnVar = '#3AA7E5'; //64,102,226
   } else if (4<=downloadBitrate && downloadBitrate<7) {
       returnVar = '#3E82E3'; //64,102,226
   } else if (1<=downloadBitrate && downloadBitrate<4) {
       returnVar = '#4160E1'; //64,102,226
   } else {
       returnVar = '#4A45E0'; //74,69,224
   }
   return returnVar;
}

function roundTripColor(roundTripTime){
  var returnVar;
  if (roundTripTime <= 10) {
      returnVar = '#FF0000'; //255,0,0
  } else if ( 10 <= roundTripTime && roundTripTime < 20 ) {
      returnVar = '#FD3C04'; //252,71,5
  } else if ( 20 <= roundTripTime && roundTripTime < 30 ) {
      returnVar = '#FB7508'; //250,139,10
  } else if ( 30 <= roundTripTime && roundTripTime < 40 ) {
      returnVar = '#F9AC0C'; //248,203,15
  } else if ( 40 <= roundTripTime && roundTripTime < 50 ) {
      returnVar = '#F8E010'; //229,246,20
  } else if ( 50 <= roundTripTime && roundTripTime < 60 ) {
      returnVar = '#DBF615'; //168,244,25
  } else if ( 60 <= roundTripTime && roundTripTime < 70 ) {
      returnVar = '#A9F419'; //111,242,29
  } else if ( 70 <= roundTripTime && roundTripTime < 80 ) {
      returnVar = '#79F21D'; //58,240,34
  } else if ( 80 <= roundTripTime && roundTripTime < 90 ) {
      returnVar = '#4CF121'; //39,238,69
  } else if ( 90 <= roundTripTime && roundTripTime < 100 ) {
      returnVar = '#24EF28'; //43,236,125
  } else if ( 100 <= roundTripTime && roundTripTime < 150 ) {
      returnVar = '#28ED58'; //48,234,177
  } else if ( 150 <= roundTripTime && roundTripTime < 200 ) {
      returnVar = '#2CEC85'; //52,232,225
  } else if ( 200 <= roundTripTime && roundTripTime < 250 ) {
      returnVar = '#30EAB0'; //56,190,230
  } else if ( 250 <= roundTripTime && roundTripTime < 300) {
      returnVar = '#33E8D8'; //60,144,228
  } else if ( 300 <= roundTripTime && roundTripTime < 350) {
      returnVar = '#37CEE6'; //64,102,226
  } else if ( 350 <= roundTripTime && roundTripTime < 400) {
      returnVar = '#3AA7E5'; //64,102,226
  } else if ( 400 <= roundTripTime && roundTripTime < 450) {
      returnVar = '#3E82E3'; //64,102,226
  } else if ( 450 <= roundTripTime && roundTripTime < 500) {
      returnVar = '#4160E1'; //64,102,226
  } else {
      returnVar = '#4A45E0'; //74,69,224
  }
  return returnVar;
}

function rssiColor(rssi){
 var returnVar;
 if (rssi >= -51) {
     returnVar = '#FF0000'; //255,0,0
 } else if (-54<=rssi && rssi<-51) {
     returnVar = '#FD3C04'; //252,71,5
 } else if (-58<=rssi && rssi<-54) {
     returnVar = '#FB7508'; //250,139,10
 } else if (-62<=rssi && rssi<-58) {
     returnVar = '#F9AC0C'; //248,203,15
 } else if (-66<=rssi && rssi<-62) {
     returnVar = '#F8E010'; //229,246,20
 } else if (-70<=rssi && rssi<-66) {
     returnVar = '#DBF615'; //168,244,25
 } else if (-74<=rssi && rssi<-70) {
     returnVar = '#A9F419'; //111,242,29
 } else if (-78<=rssi && rssi<-74) {
     returnVar = '#79F21D'; //58,240,34
 } else if (-82<=rssi && rssi<-78) {
     returnVar = '#4CF121'; //39,238,69
 } else if (-86<=rssi && rssi<-82) {
     returnVar = '#24EF28'; //43,236,125
 } else if (-90<=rssi && rssi<-86) {
     returnVar = '#28ED58'; //48,234,177
 } else if (-94<=rssi && rssi<-90) {
     returnVar = '#2CEC85'; //52,232,225
 } else if (-98<=rssi && rssi<-94) {
     returnVar = '#30EAB0'; //56,190,230
 } else if (-102<=rssi && rssi<-98) {
     returnVar = '#33E8D8'; //60,144,228
 } else if (-104<=rssi && rssi<-102) {
     returnVar = '#37CEE6'; //64,102,226
 } else if (-108<=rssi && rssi<-104) {
     returnVar = '#3AA7E5'; //64,102,226
 } else if (-112<=rssi && rssi<-108) {
     returnVar = '#3E82E3'; //64,102,226
 } else if (-116<=rssi && rssi<-112) {
     returnVar = '#4160E1'; //64,102,226
 } else {
     returnVar = '#4A45E0'; //74,69,224
 }
 return returnVar;
}

app.service('applyFilters', function(NgMap, filters, mapMarkers, $http) {
    function gridHandler(map, http){
      mapMarkers.clearMarkers();
      queryParams = filters.get();
      queryParams.zoom = parseInt(map.getZoom());
      queryParams.ne_lat = parseFloat(map.getBounds().getNorthEast().lat());
      queryParams.ne_lng = parseFloat(map.getBounds().getNorthEast().lng());
      queryParams.sw_lat = parseFloat(map.getBounds().getSouthWest().lat());
      queryParams.sw_lng = parseFloat(map.getBounds().getSouthWest().lng());
      http.get(getGridURL,
          { params:  queryParams}
      ).then(function successCallback(response) {
          var stationaryuxProbes = new Array();
          if (response['data']['requested']['keyPerformanceIndicator'] == "RSSI" || response['data']['requested']['keyPerformanceIndicator'] == "RSCP" || response['data']['requested']['keyPerformanceIndicator'] == "RSRP"){
            map.controls[google.maps.ControlPosition.RIGHT_CENTER].clear();
            map.controls[google.maps.ControlPosition.RIGHT_CENTER].push(colorScaleRssi());
          } else if (response['data']['requested']['keyPerformanceIndicator'] == "Download Speed" || response['data']['requested']['keyPerformanceIndicator'] ==  "Upload Speed"){
            map.controls[google.maps.ControlPosition.RIGHT_CENTER].clear();
            map.controls[google.maps.ControlPosition.RIGHT_CENTER].push(colorScaleDownload());
          } else if (response['data']['requested']['keyPerformanceIndicator'] == "Round Trip Time"){
            map.controls[google.maps.ControlPosition.RIGHT_CENTER].clear();
            map.controls[google.maps.ControlPosition.RIGHT_CENTER].push(colorScaleRTT());
          }
          NgMap.getMap({id: "mobileProbesMap"}).then(function (map) {
              JSON.parse(response['data']['rows']).forEach(function(cell){
                if (response['data']['requested']['keyPerformanceIndicator'] == "RSSI" || response['data']['requested']['keyPerformanceIndicator'] == "RSCP" || response['data']['requested']['keyPerformanceIndicator'] == "RSRP"){
                  color = rssiColor(cell[4]);
                } else if (response['data']['requested']['keyPerformanceIndicator'] == "Download Speed" || response['data']['requested']['keyPerformanceIndicator'] ==  "Upload Speed"){
                  color = downloadBitrateColor(cell[4]);
                } else if (response['data']['requested']['keyPerformanceIndicator'] == "Round Trip Time"){
                  color = roundTripColor(cell[4]);
                }
                var rectangle = new google.maps.Rectangle({
                  strokeColor: color,
                  strokeOpacity: 1,
                  strokeWeight: 3,
                  fillColor: color,
                  fillOpacity: 0,
                  map: map,
                  bounds: {
                    north: cell[2],
                    south: cell[3],
                    east: cell[0],
                    west: cell[1]
                  }
                });
                mapMarkers.save(rectangle);
              });
          });
      }, function errorCallback(response) {
          alert('Some error occured');
      });
    }
    this.apply = function () {
        this.filters = filters.get();
        console.log(this.filters);
        if (this.filters.mode == "main"){
            console.log('Main Window View');

        } else if(this.filters.mode == "staticProbes"){
            console.log('Stationary Probes View');
            $('.active').removeClass('active');
            $('a[href^="#!stationaryProbes"]').addClass('active');

        } else if(this.filters.mode == "mobileProbes"){
            console.log('Mobile Probes View');
            NgMap.getMap({id: "mobileProbesMap"}).then(function (map) {
                google.maps.event.clearListeners(map, 'zoom_changed');
                google.maps.event.addListener(map, 'zoom_changed', function() {
                  gridHandler(map, $http);
                  //console.log('zoom: ' + map.getZoom() + ' center lat: ' + map.getCenter().lat() + ' center lng: ' + map.getCenter().lng());
                });
                gridHandler(map, $http);
            });
        }
    }
});
