app.controller('mobileProbesController',
    function (NgMap, $scope, localStorageService, $http, NavigatorGeolocation, GeoCoder, url, filters, $controller, $compile, filtersSetters, citiesOnMap) {

        $('.active').removeClass('active');
        $('a[href^="#!mobileProbes"]').addClass('active');

        filters.save('mode', 'mobileProbes');

        $scope.uxMobileProbes = [];

        //MAP Default Settings
        $scope.center = "[40.0285681536870, -7.67559350574607]";
        $scope.zoom = 7;
        $scope.mapType = "HYBRID";
        $scope.mapStyle = [
        ];

        $scope.url = url;
        $scope.cities = new Array();
        //$scope.citiesMarkersMobile   = localStorageService.citiesWithMarkersMobile || [];
        $scope.citiesMarkersMobile = new Array();
        $scope.googleMapsCityIcon = $scope.url.logoUrl;
        $scope.uxStaticProbeIcon = $scope.url.staticProbeIcon;
        $scope.staticProbesUrl = $scope.url.staticProbesGetUrl;
        $scope.googleLogoUrl = $scope.url.googleLogoUrl;
        $scope.youtubeLogoUrl = $scope.url.youtubeLogoUrl;
        $scope.facebookLogoUrl = $scope.url.facebookLogoUrl;
        $scope.genericImageUrl = $scope.url.genericImageUrl;

        $scope.selectedCity = filters.get()['city'];
        $scope.selectedCountry = filters.get()['country'];

        $scope.setCity = function (city, country, latitude, longitude){
          var filtersState = filters.get();
          filtersSetters.setCountry(country, $scope, 'mobileProbes');
          filtersSetters.setCityCountry(city, $scope);
          NgMap.getMap({id: "mobileProbesMap"}).then(function (map) {
            angular.forEach($scope.citiesMarkersMobile, function (marker) {
              marker.setMap(null);
            });
            $scope.citiesMarkersMobile = [];
            map.setCenter(new google.maps.LatLng(latitude, longitude));
            map.setZoom(14);
            filtersSetters.setMode('mobileProbes');
          });
        };

        if($scope.selectedCity != null && $scope.selectedCountry != null){
          GeoCoder.geocode({address: $scope.selectedCity + ', ' + $scope.selectedCountry})
          .then(function (result) {
            $scope.setCity($scope.selectedCity, $scope.selectedCountry, result[0].geometry.location.lat(), result[0].geometry.location.lng());
          });
        } else {
          //MAP Initialization
          NgMap.getMap({id: "mobileProbesMap"}).then(function (map) {
            $http.get($scope.url.mobileProbesGetUrl).then(function successCallback(response) {
              angular.forEach(JSON.parse(response.data.citiesWithMeasurements), function (city) {
                GeoCoder.geocode({address: city.name + ', ' + city.country})
                .then(function (result) {
                  var marker = new google.maps.Marker({
                    position: new google.maps.LatLng(result[0].geometry.location.lat(), result[0].geometry.location.lng()),
                    map: map,
                    name: city.name,
                    country: city.country,
                    icon: $scope.googleMapsCityIcon
                  });
                  marker.addListener('click', function () {
                    //$scope.stationaryUxProbeClicked(uxProbe);
                    $scope.setCity(city.name, city.country, result[0].geometry.location.lat(), result[0].geometry.location.lng());
                    $('.active').removeClass('active');
                    $('a[href^="#!mobileProbes"]').addClass('active');
                  });
                  $scope.citiesMarkersMobile.push(marker);
                });
              });
              citiesOnMap.setCitiesOnMap($scope.citiesMarkersMobile);
            }, function errorCallback(response) {
                alert('Some error occured');
            });
          });
        }
    });
