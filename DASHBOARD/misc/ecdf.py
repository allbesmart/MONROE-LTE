#Diference between CDF and ECDF is CDF is a theoreticall construct and the ECDF is based on actual data (measurements)

import numpy as np
import matplotlib.pyplot as plt

def ecdf(data):
    data_size=len(data)
    data_set=sorted(set(data))
    bins=np.append(data_set, data_set[-1]+1)
    counts, bin_edges = np.histogram(data, bins=bins, density=False)
    counts=counts.astype(float)/data_size
    cdf = np.cumsum(counts)

    ecdf = [[x,y] for x,y in zip(bin_edges[0:-1], cdf)]

    print(ecdf)
    #for row in ecdf:
    #    print(row)

    # ###Just For Plotting the Image
    #

    #plt.plot(bin_edges[0:-1], cdf,linestyle='--', marker="o", color='b')
    #plt.ylim((0,1))
    #plt.ylabel("ECDF")
    #plt.grid(True)
    #plt.show()

def main():
    data = np.genfromtxt('select_rssi_from_device_modem_measuremen.csv',delimiter=',')
    ecdf(data)

if __name__ == '__main__':
    main()
