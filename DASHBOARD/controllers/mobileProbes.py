import json
import math
import time
import psycopg2
import psycopg2.extras
from operator import itemgetter

def getColorForOperator(operator):
    if operator == 'MEO/TMN':
        return '#2E86C1'
    if operator == 'Vodafone':
        return '#FF5733'
    if operator == 'NOS/Optimus':
        return '#17202A'
    else:
        return '#3A3A3A'


def index():
    return dict(message='404')


def get_cities_with_measurements():
    response.view = 'generic.json'
    return dict(
        citiesWithMeasurements=json.dumps(db(db.cities_with_measurements).select(db.cities_with_measurements.ALL).as_list())
    )

@request.restful()
def api():
    def GET(*args, **vars):
        def filtersTechnologyHandler(technologyArray):
            technologyQuery = "and ( device_mode = {twoG} or device_mode = {threeG} or device_mode = {fourG} or device_mode = {fiveG} )".format(
                twoG=3 if technologyArray[0] == 'true' else -1,
                threeG=4 if technologyArray[1] == 'true' else -1,
                fourG=5 if technologyArray[2] == 'true' else -1,
                fiveG=-1 if technologyArray[3] == 'true' else -1)
            return technologyQuery

        def gridSquareSize(zoomLevel):
            factor = 2
            squareSize = 0
            inititalSize = 128000

            if zoomLevel == 5:
                squareSize = inititalSize/1/factor
            elif zoomLevel == 6:
                squareSize = inititalSize/2/factor
            elif zoomLevel == 7:
                squareSize = inititalSize/4/factor
            elif zoomLevel == 8:
                squareSize = inititalSize/8/factor
            elif zoomLevel == 9:
                squareSize = inititalSize/16/factor
            elif zoomLevel == 10:
                squareSize = inititalSize/32/factor
            elif zoomLevel == 11:
                squareSize = inititalSize/64/factor
            elif zoomLevel == 12:
                squareSize = inititalSize/128/factor
            elif zoomLevel == 13:
                squareSize = inititalSize/256/factor
            elif zoomLevel == 14:
                squareSize = inititalSize/512/factor
            elif zoomLevel == 15:
                squareSize = inititalSize/1024/factor
            elif zoomLevel == 16:
                squareSize = inititalSize/2048/factor
            elif zoomLevel == 17:
                squareSize = inititalSize/4096/factor
            elif zoomLevel == 18:
                squareSize = inititalSize/8192/factor
            elif zoomLevel >= 19:
                squareSize = inititalSize/16384/factor
            return int(squareSize)

        response.view = 'generic.json'
        if args[0]:
            if len(args) > 0 and args[0] == 'getGrid':
                try:
                    #print (args)
                    #print("\n")
                    #print(vars)
                    conn_string = "host='127.0.0.1' dbname='blueprobe_dashboard' user='tiagoalves' password='t1i2a3g4o5'"
                    dbConnection = psycopg2.connect(conn_string)
                    with dbConnection.cursor(cursor_factory=psycopg2.extras.DictCursor) as cursor:
                        if vars['technologyPicker']:
                            deviceModeQuery = ''

                            technologyQuery = filtersTechnologyHandler(vars['technologyPicker'])
                            #print('technologyQuery: {}\n'.format(technologyQuery))

                            #print('mcc query: {}\n'.format(int(json.loads(vars['operator'])['mcc'])))

                            i = 0
                            mncQuery = 'and ( '
                            for mnc in json.loads(vars['operator'])['mnc']:
                                i += 1
                                mncQuery = mncQuery + 'mnc = {}'.format(mnc)
                                if len(json.loads(vars['operator'])['mnc']) > 1:
                                    if i < len(json.loads(vars['operator'])['mnc']):
                                        mncQuery = mncQuery +  ' or '
                            mncQuery = mncQuery + ' )'
                            #print('mncQuery: {}\n'.format(mncQuery))

                            timeQuery = "and time_stamp >= {startTime} and time_stamp <= {endTime} ".format(
                                startTime=vars['startTimePicker'] if 'startTimePicker' in vars else int(time.time() - 2592000),
                                endTime=vars['endTimePicker'] if 'endTimePicker' in vars else int(time.time()))
                            #print('timeQuery: {}\n'.format(timeQuery))

                            squareSize = gridSquareSize( int(vars['zoom']) )
                            #print('requested square size: {}\n'.format(squareSize))

                            #print('ne coords: {},{}\n'.format(float(vars['ne_lat']), float(vars['ne_lng'])))
                            #print('sw coords: {},{}\n'.format(float(vars['sw_lat']), float(vars['sw_lng'])))

                            #print('Key Performance Indicator: {}\n'.format(vars['keyPerformanceIndicator']))
                            kpi = ''
                            tableByKpi = ''
                            cellValueByKpi = ''
                            innerValueByKpi = ''
                            if vars['keyPerformanceIndicator'] == 'RSSI':
                                #print('KPI IS RSSI')
                                kpi = 'rssi_metric_units'
                                tableByKpi = 'device_modem_measurements'
                                innerValueByKpi = 'avg(rssi_metric_units)'
                                cellValueByKpi = 'cast(round(10.*log(cell.avg_area)) as DOUBLE PRECISION)'
                            elif vars['keyPerformanceIndicator'] == 'RSCP':
                                #print('KPI IS RSCP')
                                kpi = 'rscp_metric_units'
                                tableByKpi = 'device_modem_measurements'
                                innerValueByKpi = 'avg(rscp_metric_units)'
                                cellValueByKpi = 'cast(round(10.*log(cell.avg_area)) as DOUBLE PRECISION)'
                            elif vars['keyPerformanceIndicator'] == 'RSRP':
                                #print('KPI IS RSRP')
                                kpi = 'rsrp_metric_units'
                                tableByKpi = 'device_modem_measurements'
                                innerValueByKpi = 'avg(rsrp_metric_units)'
                                cellValueByKpi = 'cast(round(10.*log(cell.avg_area)) as DOUBLE PRECISION)'
                            elif vars['keyPerformanceIndicator'] == 'RSRQ':
                                #print('KPI IS RSRP')
                                kpi = 'rsrq_metric_units'
                                tableByKpi = 'device_modem_measurements'
                                innerValueByKpi = 'avg(rsrq_metric_units)'
                                cellValueByKpi = 'cast(round(10.*log(cell.avg_area)) as DOUBLE PRECISION)'
                            elif vars['keyPerformanceIndicator'] == 'Download Speed':
                                #print('KPI IS DW SPEED')
                                kpi = 'downlink_bitrate'
                                tableByKpi = 'device_tcp_download_measurements'
                                innerValueByKpi = 'avg(downlink_bitrate)'
                                cellValueByKpi = 'cast(round((cell.avg_area/1000000)) as DOUBLE PRECISION)'
                            elif vars['keyPerformanceIndicator'] == 'Upload Speed':
                                #print('KPI IS UL SPEED')
                                kpi = 'uplink_bitrate'
                                tableByKpi = 'device_tcp_uplink_measurements'
                                innerValueByKpi = 'avg(uplink_bitrate)'
                                cellValueByKpi = 'cast(round((cell.avg_area/1000000)) as DOUBLE PRECISION)'
                            elif vars['keyPerformanceIndicator'] == 'Round Trip Time':
                                #print('KPI IS Round Trip Time')
                                kpi = 'round_trip_time'
                                tableByKpi = 'device_rtt_measurements'
                                innerValueByKpi = 'avg(round_trip_time)'
                                cellValueByKpi = 'cast(round((cell.avg_area)) as DOUBLE PRECISION)'

                            query = "SELECT cell.x_max, cell.x_min, cell.y_max, cell.y_min, {cellValue} FROM (SELECT st_xmax(cell) AS x_max, st_xmin(cell) AS x_min, st_ymax(cell) AS y_max, st_ymin(cell) AS y_min, ST_Y(ST_TRANSFORM(st_centroid(cell),4326)) AS longitude, (SELECT {innerValue} FROM {table} WHERE geo_position && ST_MakeEnvelope(ST_Y((st_makepoint( st_xmax(st_extent(cell)), st_ymax(st_extent(cell)) ))), ST_X((st_makepoint( st_xmin(st_extent(cell)), st_ymin(st_extent(cell)) ))), ST_Y((st_makepoint( st_xmin(st_extent(cell)), st_ymin(st_extent(cell)) ))), ST_X((st_makepoint( st_xmax(st_extent(cell)), st_ymax(st_extent(cell)) ))),  4326) {time_query} AND mcc = {} {} {device_mode_query} AND {kpi} != 0) AS \"avg_area\" FROM (SELECT ( ST_Dump(makegrid_2d(ST_GeomFromText('Polygon(('|| {} || ' ' || {} || ', ' || {} || ' ' || {} || ', ' || {} || ' ' || {} || ', ' || {} || ' ' || {} || ', ' || {} || ' ' || {} || '))', 4326), {}, 32662) )).geom AS cell) AS q_grid GROUP BY cell ORDER BY avg_area ASC ) AS cell WHERE cell.avg_area IS NOT NULL".format(int(json.loads(vars['operator'])['mcc']), mncQuery, float(vars['ne_lng']), float(vars['ne_lat']),  float(vars['ne_lng']),  float(vars['sw_lat']), float(vars['sw_lng']), float(vars['sw_lat']), float(vars['sw_lng']), float(vars['ne_lat']), float(vars['ne_lng']), float(vars['ne_lat']), squareSize, device_mode_query = deviceModeQuery, time_query = timeQuery, cellValue = cellValueByKpi, innerValue = innerValueByKpi, table = tableByKpi, kpi=kpi )

                            # print(query)
                            #print("\n\nQuery: {}\n\n".format(query))

                            cursor.execute(query)
                            resultingRows = cursor.fetchall()
                            #print(resultingRows)

                        return dict( rows = json.dumps( resultingRows ), requested=vars )
                except Exception as getGridError:
                    return dict( error=getGridError )
                # try:

                #     if technologiesArray[2] == "true" and technologiesArray[3] == "true" and technologiesArray[4] == "true":
                #         # integratedView
                #         defaultDeviceModeQuery = ""
                #     # if technologiesArray[3] == "false" and technologiesArray[4] == "false" and technologiesArray[5] == "false":
                #     #     #integratedView
                #     #     defaultDeviceModeQuery = ""
                #     defaultTimeQuery = "and time_stamp >= {startTime} and time_stamp <= {endTime} ".format(
                #         startTime=vars['startTime'] if int(vars['startTime']) != 0 else int(time.time() - 2592000),
                #         endTime=vars['endTime'] if int(vars['endTime']) != 0 else int(time.time()))
                #     query = "SELECT cell.x_max, cell.x_min, cell.y_max, cell.y_min, cast(round(10.*log(cell.avg_rssi_area)) as DOUBLE PRECISION) FROM (SELECT st_xmax(cell) AS x_max, st_xmin(cell) AS x_min, st_ymax(cell) AS y_max, st_ymin(cell) AS y_min, ST_Y(ST_TRANSFORM(st_centroid(cell),4326)) AS longitude, (SELECT avg(rssi_metric_units) FROM device_modem_measurements WHERE device_modem_measurements.geo_position && ST_MakeEnvelope(ST_Y((st_makepoint( st_xmax(st_extent(cell)), st_ymax(st_extent(cell)) ))), ST_X((st_makepoint( st_xmin(st_extent(cell)), st_ymin(st_extent(cell)) ))), ST_Y((st_makepoint( st_xmin(st_extent(cell)), st_ymin(st_extent(cell)) ))), ST_X((st_makepoint( st_xmax(st_extent(cell)), st_ymax(st_extent(cell)) ))),  4326) {time_query} AND device_modem_measurements.mcc = {} and device_modem_measurements.mnc = {mncQuery} {device_mode_query}) AS \"avg_rssi_area\" FROM (SELECT ( ST_Dump(makegrid_2d(ST_GeomFromText('Polygon(('|| {} || ' ' || {} || ', ' || {} || ' ' || {} || ', ' || {} || ' ' || {} || ', ' || {} || ' ' || {} || ', ' || {} || ' ' || {} || '))', 4326), {}) )).geom AS cell) AS q_grid GROUP BY cell ORDER BY avg_rssi_area ASC ) AS cell WHERE cell.avg_rssi_area IS NOT NULL".format(
                #         int(vars['mcc']), mncQuery=mncQuery, float(vars['ne_lat']), float(vars['ne_lng']), float(vars['ne_lat']), float(vars['sw_lng']), float(vars['sw_lat']), float(vars['sw_lng']),
                #         float(vars['sw_lat']), float(vars['ne_lng']), float(vars['ne_lat']), float(vars['ne_lng']), int(vars['squareSize']), device_mode_query=defaultDeviceModeQuery, time_query=defaultTimeQuery)
                #     raw_rows = db.executesql(query)
                #     return dict(rssiAreaGrid=json.dumps(raw_rows))
                # except Exception as error:
                #     print(error)
                #     return dict(error=str(error))
        else:
            return dict(error="No Input Arguments")
        return dict(error="Something bad happened!")

    return locals()
