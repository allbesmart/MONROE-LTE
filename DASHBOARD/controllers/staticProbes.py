import json
import math
import time
import psycopg2
import numpy as np
import psycopg2.extras
from operator import itemgetter

def index():
    return dict(message='404')


def get_static_probes():
    response.view = 'generic.json'
    return dict(
        citiesWithMeasurements=json.dumps(db(db.cities_with_measurements).select(db.cities_with_measurements.ALL).as_list()),
        uxStationaryProbes=json.dumps(db(db.fixed_probes_by_city).select(db.fixed_probes_by_city.ALL).as_list()),
        uxClusters=json.dumps(db(db.clusters).select(db.clusters.ALL).as_list())
    )